# How to make space on Linux

These are a serie of tips to make space on Linux when your hard drive is running out.

## Remove cache and other stuff using BleachBit

BleachBit is like a open-source CCleaner for Linux. You can choose what you want to remove on the left panel and you need to click on the red button to apply everything.

## Scan your drive using NCDU

NCDU is an application like `baobab` but in your terminal.

```shell
cd /
sudo ncdu
```

## Clean your APT cache

This is something you can do using BleachBit but you can also do it manually using:

```shell
sudo apt-get clean
```
