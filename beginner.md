# A Beginners Guide to Linux

## Linux offers a different take on desktop computing than Windows and macOS

The Linux operating system offers a rich mix of features and security

## What Is Linux?

Linux powers a variety of computer systems from light bulbs to guns, laptops to large computer centers. Linux powers everything from your phone to your smart regrigerator.

In desktop computing, Linux provides an alternative to commercial operating systems such as Windows and macOS. 

Linux sources from some of the earliest computer operating systems 
from the 1960s and 1970s, and so it retains its root philosophies of 
strong user-level security, customization, and system stability.

## Why Use Linux Instead of Windows or macOS?

There are many reasons why you would use Linux instead of Windows or macOS and here are just a few of them:

- Linux is supported on older computers. While Windows XP will still run on older hardware it is no longer supported, so there 
  are no security updates. Several Linux distributions focus on older 
  hardware and are maintained and updated regularly. And Macs? The OS and 
  the hardware are so tightly tied that you cannot run modern macOS on old
   Mac machines — although you're free to run Linux on those old Apple 
  computers!
- Some Linux distributions and desktop environments are
   now more familiar to long-time computer hobbyists than Windows 8 and 
  Windows 10. If you like the Windows 7 look and feel, why not try Linux Mint instead?
- A
   typical Linux distribution comes in at just over 1 gigabyte although 
  you can get some which are just a few hundred megabytes. Windows and 
  macOS require at least a DVD's worth of bandwidth.
- Linux has always been more secure than Windows and there are very few viruses for Linux.
- Windows regularly phones home with data gathered through Cortana and search in general. While not a new thing and clearly Google does 
  the same thing, Linux isn't doing the same — especially if you choose a 
  free community distribution. And Apple's hardware and software are so 
  intertwined, particularly with the App Store, that Apple knows virtually
   everything you do.
- You can make Linux look, feel and behave exactly as you
   want it to. With Windows and macOS, the computer behaves exactly as 
  Microsoft or Apple thinks it should.

## Which Linux Distribution Should You Use?

The Linux kernel is like an engine. A *distribution* is an actual vehicle that houses the engine.

So which Linux distribution should you choose? Linux supports several hundred distributions, each optimized for some specific use case:

- Linux Mint: Requires low computer expertise, easy to install, easy to use and has a familiar-looking desktop for Windows users.
- Debian: For those seeking a truly free Linux distribution with no proprietary drivers, firmware or software then Debian is for you.
- Ubuntu: A modern Linux distribution that is easy to install and easy to use.
- openSUSE: A stable and powerful Linux distribution. Not as easy to install as Mint and Ubuntu but a very good alternative nonetheless.
- Fedora: The most up-to-date Linux distribution with all new concepts incorporated at the earliest possible opportunity.
- Mageia: Rose from the ashes of the formerly great Mandriva Linux. Easy to install and easy to use.
- CentOS: As with Fedora, CentOS is based on the commercial Linux distribution, Red Hat Linux. Unlike Fedora, it is built for stability.
- Manjaro: Based on Arch Linux, Manjaro provides a great balance between ease of use and up to date software.
- LXLE: Based on the lightweight Lubuntu distribution this provides a fully-featured Linux distribution for older hardware.
- Arch:
   A rolling release distribution, meaning that you don't have to install 
  new versions of the operating system at any point because it updates 
  itself. More difficult for the new user to get to grips with but very 
  powerful.
- Elementary: Linux for people who like a Mac-style interface.
- XUbuntu: A distribution based on Ubuntu that runs XFCE as *dekstop environement*. It uses pannels and menus and is very lightweight like LXLE.
- QubeOS: Requires more understanding but still less has to use than Arch. It is the most secure OS in the world for privacy and security.

## How to Run Linux From a DVD or USB

A live Linux DVD or USB lets you run Linux without installing it to 
your hard drive. This basically lets you test drive Linux before 
committing to it and is also good for the occasional user.

Most distributions use a live loader to both test and install the 
distribution. Ubuntu Linux, a common choice for new Linux hobbyists, 
offers an excellent live environment.

To make a bootable USB you can use the software Etcher:

1. Download the ISO file of the distro you want to install
2. Install the software `Etcher`
3. Backup a flash drive
4. In Etcher, select the file and the flash drive
5. 	Once the USB key is flashed, shutdown the computer and press all the F row (F1, F2, etc) during the boot. 
	This should start a bootloader, where you can select your key. If it doesn't, try to get more informations about how to access to the bootloader of your system.
	These are also common solve tips to access the bootloader:
		* Disable secureboot
		* Switch to Legacy boot

Now you can try your system on the flash drive.

## How to Install Linux

Each Linux distribution relies upon a different *installer*, 
which is a program that guides you through configuring Linux. In most 
cases, you're free to install Linux as the new operating system on a 
computer, or as a separate OS that doesn't overwrite Windows.

You will need to have created a bootable USB stick using the steps of the section  above. Or you can also order a computer with Linux pre-built. This will support the devloppers and you will not need to install Linux and all the drivers that you could need. 

If you choose to install it on your own, in most cases, you will just have to follow the few steps. You can make a dualboot to don't need to backup your harddrive, but this will divide the harddrive space.

Otherwise you can make a backup of your system, then install Linux completely. This is the recommended option. Notice that it is possible that you need to install a lot of drivers depending on your hardware.

## What Is a Desktop Environment?

A typical [Linux distribution includes several different components](https://www.lifewire.com/linux-desktop-environment-explained-4121640).

A *display manager* logs you in while a *window manager* governs windows, panel, menus, dash interfaces and core applications. Many of these items are bundled together to make a *desktop environment*.

Some Linux distributions ship with just one desktop environment 
(although others are available in the software repositories), while 
others offer different versions of the distribution fine-tuned for 
different desktop environments.

* Common desktop environments include Cinnamon, GNOME, Unity, KDE, Enlightenment, XFCE, LXDE and MATE.

* Cinnamon is a more conventional desktop environment that looks much 
  like Windows 7, with a panel at the bottom, a menu, system tray icons, 
  and quick launch icons. 

* GNOME and Unity are fairly similar. They are modern desktop 
  environments that use the concept of launcher icons and a 
  dashboard-style display for picking applications. There are also core 
  applications that integrate well with the overall theme of the desktop 
  environment.

* KDE is a fairly classic-style desktop environment with many custom 
  features and a core set of applications that are all highly 
  customizable.

* Enlightenment, XFCE, LXDE, and MATE are lightweight desktop environments with panels and menus.

## Are There Any Decent Office Suites for Linux?

For personal use and for small- to medium-sized businesses, LibreOffice presents a strong alternative to Microsoft Office, for free.

LibreOffice comes with a word processor with the majority of the 
features you expect from a word processor. It also features a decent 
spreadsheet tool which again is fully featured and even including a 
basic programming engine although it isn't compatible with Excel VBA.

Other tools include the presentation, maths, database and drawing packages which are all very good.

## How to Install Software Using Linux

Linux does not install software the same way that Windows does. A *package manager* accesses repositories that archive various software applications that 
work on a given distribution. The package management tool provides a 
mechanism to search for software, install software, keep the software up
 to date and remove the software.

Each distribution provides its own graphical tool. There are common command-line tools used by many different distributions.

For example, Ubuntu, Linux Mint, and Debian all use the apt-get package manager. Fedora and CentOS use the yum package manager. Arch and Manjaro use Pacman.

Notice that all the softwares are not available there, it doesn't mean that you can't install them! These are some common file types:

* DEB: Deb is the packaging format of Ubuntu and Debian based distributions. Those packages are also used in APT. Usually you can just click on it, and it should open it in the app store, where you will be able to install it. If you don't like the app store you can also use `gdebi`

* AppImage: AppImage is a format that is very used because it can be ran on all Linux distributions. To use an AppImage you just need to click on it, or place it on your desktop or dock to get it access more quickly. You can also create a shortcut to it by using the tool `menulibre` . If clicking on it doesn't work, you can try to click right > Permissions > Allow executing file as program. 

* BINary files: Binary files can be installed the same way as AppImage files.

* TAR files: These can be multiple things and often have a file called `README.md` that explains how to install it. If it is the file provided for installation there is a high chance that a binary is inside, where you will be able to do the same steps as described above.

* JAR files: These are Java files, to use them you will need the package `openjdk8-jre` or simillar. Once you have it, you can use it like a AppImage file. 

* RPM and other packages: It is possible that you encounter other types of packages, if you do, you can use a tool called `alien` to convert them into the right format for your distribution. LINK HERE

## The Linux Command Line

Given its long heritage and the diversity of approach of modern desktop environments, a lot of Linux still works from a *shell session*. In the macOS world, these sessions are called the *terminal;* in Windows, the *Command Prompt*.

Although the graphical user interface of modern Linux DEs can do just
 about everything, much online education about Linux relies on the shell
 because it's not tied to the peculiarities of a given distribution or 
window manager. People new to Linux can get away with rarely, or never, 
working from the shell, but people who grow to love Linux often go to 
the shell *first* because of how easy it is to type one command instead of clicking through many different menus.

If you want some nice examples of  shell commands on Linux, these are some:

| Purpose                                                     | Command                                    |
| -----------------------------------------------------------:| ------------------------------------------ |
| Create a folder                                             | `mkdir <name>`                             |
| Open a folder                                               | `cd <name>`                                |
| List all the files and directories in the current folder    | `ls`                                       |
| Get the path to the current folder                          | `pwd`                                      |
| Run a command as root                                       | `sudo <command>`                           |
| Moving a file / directory                                   | `mv <origin path> <future path>`           |
| Copy a file                                                 | `cp <origin> <where to copy>`              |
| Copy a directory                                            | `cp -r <origin> <where to copy>`           |
| Find a file                                                 | `find -iname "<filename>"`                 |
| Run a program                                               | `<program>`                                |
| See what processes takes most of your CPU                   | `top`                                      |
| Kill a process                                              | `kill <id>` or `pkill <name>`              |
| Clear the screen                                            | <kbd>CTRL</kbd> + <kbd>L</kbd> or `clear`  |
| Get information about the usage of a program                | `man <program>`                            |
| See system infos                                            | `neofetch`                                 |
| Installing a APT program (for debian and ubuntu based)      | `sudo apt install <name>`                  |
| Removing a APT or DEB program (for debian and ubuntu based) | `sudo apt remove <name>`                   |
| Installing a DEB file                                       | `sudo dpkg -i <file>`                      |
| Killing the current program process                         | <kbd>CTRL</kbd> + <kbd>C</kbd>             |
| Running a binary or a AppImage                              | `chmod +x <path to file>` `<path to file>` |
| Write things in your terminal                               | `nano <filename>` or `micro <filename>`    |
| Scan your hardrive                                          | `ncdu`                                     |

## Warning 

I am not gonna lie. Linux can take a lot of time. It depends on what distribution you choosed, the hardware compatibility and your daily usage. 

Linux is cross-platform but sometimes, require a bit of tricks to be really fully integrated. But if you don't use your computer for *weird thing* like me (removing the filesystem, devlopping server things, etc), once your computer is configured, you don't need to do anything else than enjoying.

If you don't enjoy it, that means that something is missing in your configuration. Because you can configure everything on Linux. If you want help, feel free to post an issue, asking people using Linux or even just posting something on forums.

## Copyright

This article is comming from a post from *Lifewire* and has been improved here with some other tips.
