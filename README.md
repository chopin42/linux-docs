# Linux docs

This repo will contain informations about Linux documentation and help. 

This is not focused on one *distribution* but is made to be begginer friendly and be be adaptable for many different distributions.

## New to Linux? Start here

* [A begginer guide to Linux](./beginner.md)

## Management

* [Space management](./space-management.md)

## Privacy

* [Firefox Privacy Settings](./firefox-privacy.md)
