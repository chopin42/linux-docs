# Privacy and speed guide in Firefox

Firefox is a wonderful tool for privacy and security. It is also less CPU
consuming than Chrome based browsers.

## Settings

Go into the preferences of Firefox and set the security level to `Strict`

![](./images/firefox-settings-1.png)

Then you can also make those settings as well:

![](./images/firefox-settings-2.png)

![](./images/firefox-settings-3.png)

![](./images/firefox-settings-4.png)

![](./images/firefox-settings-5.png)


## Add uBlock origin

1. Install uBlock Origin.
2. Go into the uBlock settings: ![](./images/ublock-1.png)
3. Make the following settings: ![](./images/ublock-2.png)
4. Add new filters: ![](./images/ublock-3.png)
5. Set it to `Block all` by setting the left cell to red: ![](./images/ublock-4.png)
6. When websites are broken, simply allow some hosts by setting the right cell to grey and click on the lock icon: ![](./images/ublock-5.png)
7. If the website is still broken, allow the big medias: ![](./images/ublock-6.png)

These tricks with uBlock Origin will block adds, trackers, large medias, etc. Which makes the pages, better, safer and faster.
